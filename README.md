# Helm initial setup

### Gcloud cli setup
gcloud setup you may use [gcloud cli setup](https://gitlab.com/daniel-svoboda/devops/google-cloud-specific/gcloud-cli-setup)

### Create ServiceAccount && ClusterRole

```
echo "
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system" | kubectl create -f -
```
```
echo "
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system" | kubectl create -f -
```

### Initiate helm with correct service-account permissions
`helm init --service-account tiller`

### Use of Dockerfile
copy the Dockerfile into the helm project

### Building Helm Docker image
to build image run from within terraform files directory:
	- `docker build -t terraform_gce:v1 -f ../Dockerfile .`
to build from where Dockerfile is:
	- `docker build -t terraform_infra:v1 .`

### Run the built image
`docker run -it --rm terraform_infra:v1 /bin/sh`

### Helm install package
`helm install -n <DESIRED_DEPLOYMENT_NAME> -f <path/to//values.yaml .`

### Cleanup/helm delete package
`helm del --purge consul`
